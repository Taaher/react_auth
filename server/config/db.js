const mongoose = require("mongoose");


const connectDB = async () => {
  const connect = await mongoose.connect(process.env.URL_DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });
  console.log("Connected to Database!");
};

module.exports = connectDB;
