const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
require("dotenv").config({ path: "./server/config/config.env" });
const authRouters = require("./routes/authRouters");
const homeRouter = require("./routes/homeRouters");
const connectDB = require("./config/db");
//connection to database
connectDB();
const app = express();
//config body-parser
app.use(bodyParser.json());
//config morgan
app.use(morgan("combined"));

//config helmet
app.use(helmet());

//config cookie parser
app.use(cookieParser());

//config cors
app.use(cors());

//configuration routes
app.use("/", homeRouter);
app.use("/api/auth", authRouters);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => console.log(`server run on port ${PORT}`));
