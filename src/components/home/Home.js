import React from "react";
import { useSelector } from "react-redux";



const Home = () => {
  const auth = useSelector((state) => state.auth.authenticated);
  return (
    <div>
      
      <h2>Home!</h2>
    </div>
  );
};

export default Home;
