import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Button,
} from "reactstrap";
import { NavLink } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { signOut } from "../../redux/auth/authActions";
import "./navbar.css";

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const auth = useSelector((state) => state.auth.authenticated);
  const dispatch = useDispatch();
  return (
    <div>
      <Navbar className="navbar" dark expand="md">
        <NavbarBrand href="/">
          <img
            src="https://tznext.herokuapp.com/blueTz.svg"
            alt="tz"
            className="tz-logo"
          />
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink to="/home" className="NavLink">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/privatepage" className="NavLink">
                Private
              </NavLink>
            </NavItem>
          </Nav>
          {auth ? (
            <div>
              <Button
                onClick={() => dispatch(signOut())}
                className="btn btn-warning"
              >
                Sign out!
              </Button>
            </div>
          ) : (
            <div>
              <NavLink to="/signup" className="btn btn-warning mr-2">
                Signup!
              </NavLink>
              <NavLink to="/signin" className="btn btn-success">
                Signin!
              </NavLink>
            </div>
          )}
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Header;
