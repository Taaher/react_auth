import {
  Card,
  CardBody,
  CardTitle,
  Button,
  Form,
  Label,
  FormGroup,
  Input,
} from "reactstrap";
import { NavLink } from "react-router-dom";
import { useFormik } from "formik";
import { signIn } from "../../../redux/auth/authActions";
import { useDispatch } from "react-redux";
import * as Yup from "yup";

export default function SignIn() {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },

    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Required"),
      password: Yup.string()
        .max(32, "Must be 32 characters or less")
        .min(6, "Must be 6 characters or less")
        .required("Required"),
    }),
    onSubmit: (values) => {
      dispatch(signIn(values));
    },
  });
  return (
    <div className="row mt-4 m-4">
      <div className="mt-4 col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto">
        <Card>
          <CardBody>
            <CardTitle tag="h5">Signin Account</CardTitle>
            <Form onSubmit={formik.handleSubmit}>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="email"
                  onChange={formik.handleChange}
                  value={formik.values.email}
                />
              </FormGroup>
              {formik.errors.email ? (
                <div className="alert alert-danger">{formik.errors.email}</div>
              ) : null}
              <FormGroup>
                <Label for="password">Password</Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="password"
                  onChange={formik.handleChange}
                  value={formik.values.password}
                />
              </FormGroup>
              {formik.errors.password ? (
                <div className="alert alert-danger">
                  {formik.errors.password}
                </div>
              ) : null}
              <div>
                <Button type="submit" className="btn btn-success">
                  Signin
                </Button>

                <NavLink
                  to="/signup"
                  type="submit"
                  className="btn btn-warning ml-2"
                >
                  create account
                </NavLink>
              </div>
            </Form>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
