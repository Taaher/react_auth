import { combineReducers, createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import reduxLogger from "redux-logger";
import { authReducers } from "./auth/authReducers";

const configStore = () => {
  const store = createStore(
    combineReducers({
      auth: authReducers,
    }),
    applyMiddleware(reduxThunk, reduxLogger)
  );
  return store;
};

export default configStore;
