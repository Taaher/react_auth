import {
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  SIGNIN_SUCCESS,
  SIGNIN_FAILURE,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,
} from "./authTypes";
import axios from "axios";

import History from "../../History";

export const signUp = (user) => async (dispatch) => {
  try {
    const response = await axios.post(
      "http://localhost:8000/api/auth/signup",
      user
    );
    localStorage.setItem("token", response.data.token);
    History.push("/");
    dispatch({ type: SIGNUP_SUCCESS, payload: response.data.token });
  } catch (err) {
    dispatch({ type: SIGNUP_FAILURE, payload: err.message });
  }
};

export const signIn = (user) => async (dispatch) => {
  try {
    const response = await axios.post(
      "http://localhost:8000/api/auth/signin",
      user
    );
    localStorage.setItem("token", response.data.token);
    History.push("/");
    dispatch({ type: SIGNIN_SUCCESS, payload: response.data.token });
    console.log("checked!");
  } catch (err) {
    dispatch({ type: SIGNIN_FAILURE, payload: err.message });
  }
};

// sign out user
export const signOut = () => (dispatch) => {
  console.log("clicked signOut");
  try {
    localStorage.removeItem("token");
    dispatch({ type: LOGOUT_SUCCESS, payload: "" });
  } catch (error) {
    dispatch({ type: LOGOUT_FAILED, payload: error });
  }
};
