import {
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  SIGNIN_SUCCESS,
  SIGNIN_FAILURE,
  LOGOUT_SUCCESS,
  // LOGOUT_FAILED, //
} from "./authTypes";

const initialState = {
  authenticated: localStorage.getItem("token")
    ? localStorage.getItem("token")
    : "",
  errorMessage: "",
};

export const authReducers = (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_SUCCESS:
      return {
        ...state,
        authenticated: action.payload,
        errMess: "",
      };
    case SIGNUP_FAILURE:
      return {
        ...state,
        authenticated: "",
        errMess: action.payload,
      };
    case SIGNIN_SUCCESS:
      return {
        ...state,
        authenticated: action.payload,
        errMess: "",
      };
    case SIGNIN_FAILURE:
      return {
        ...state,
        authenticated: "",
        errMess: action.payload,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        authenticated: "",
        errorMessage: "",
      };

    default:
      return state;
  }
};
