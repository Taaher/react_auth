import { Router, Route, Switch } from "react-router-dom";
import SignIn from "./components/accounts/signin/Signin";
import SignUp from "./components/accounts/signup/Signup";
import Home from "./components/home/Home";
import Header from "./components/navbar/Navbar";
import History from "./History";
import PrivateRoute from "./components/private/PrivateRoute";
import PrivatePage from "./components/PrivatePage";

const homePage = () => {
  return <Home />;
};

export default function App() {
  return (
    <Router history={History}>
      <div>
        <Header />
        <Switch>
          <Route path="/" exact component={homePage} />
          <Route path="/signup" component={SignUp} />
          <Route path="/signin" component={SignIn} />
          <PrivateRoute path="/privatepage" exact component={PrivatePage} />
        </Switch>
      </div>
    </Router>
  );
}
